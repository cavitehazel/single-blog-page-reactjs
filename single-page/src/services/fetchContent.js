import React, {
  useState,
  useEffect
} from 'react';
import axios from 'axios'
import Content from '../components/ContentComp';

function FetchContent() {
  const [photos, setPhotos] = useState([])
  const [posts, setPosts] = useState([])

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/photos')
      .then(res => {
      })
      .catch(err => {
      })
  })

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then(res => {
        setPosts(res.data)
      })
      .catch(err => {
      })
  })

  return (
    <>
      <Content posts={posts} />
    </>
  )
}

export default FetchContent
