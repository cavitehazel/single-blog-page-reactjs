import React, {
  useState,
  useEffect
} from 'react';
import axios from 'axios'
import 'antd/dist/antd.css';
import 'antd/dist/antd.less'
import '../styles/sider.css'
import { Button, Menu, Card, Divider } from 'antd'
import {
  LeftOutlined
} from '@ant-design/icons'

function FetchSider(props) {
  const { collapsedMenu, toggleCollapsed } = props
  const [photos, setPhotos] = useState([])
  const [data, setData] = useState([])

  useEffect(() => {
    setData(photos)
  })

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/photos')
      .then(res => {
        setPhotos(res.data)
      })
      .catch(err => {
      })
  })

  useEffect(() => {
  }, [collapsedMenu])

  return (
    <div
      className="sider-div"
    >
      <Button
        className="sider-btn"
        onClick={toggleCollapsed}
      >
        <LeftOutlined />
      </Button>
      <br /><br /><br />
      <div className="menu-div">
        <Menu>
          {
            data.map((photo, index) => {
              if (photo.id < 7) {
                return (
                  <>
                    <Menu.Item className={photo.id === 1 ? "item" : "item-opacity"}>
                      <img className="thumbnail" src={photo.thumbnailUrl} />
                      <div key={index} className="menu-text">
                        <p>
                          <span className="title-text">
                            {photo.title.length > 15 ?
                              `${photo.title.substring(0, 15)}...` : photo.title
                            }
                          </span>
                          <br />
                          <span className="date-text">
                            09 OCT 2020
                            <br />
                          </span>
                        </p>
                      </div>
                    </Menu.Item>
                    <Divider
                      className="sider-divider"
                      style={
                        {
                          borderColor: '#676a6a',
                          marginLeft: '20px !important',
                          minWidth: '80% !important',
                          maxWidth: '50% !important',
                        }
                      }
                    />
                  </>
                )
              }
            })
          }
        </Menu>
      </div>
    </div>
  )
}

export default FetchSider
