import React, {
  useState,
  useEffect
} from 'react';
import { Row, Col } from 'antd';
import _ from 'lodash'
import '../styles/content.css'
import {
  FacebookOutlined,
  LinkedinOutlined,
  TwitterOutlined,
  MailOutlined
} from '@ant-design/icons'

function Content(props) {

  const { posts } = props
  const [data, setData] = useState([])
  const lowerData = [
    {
      company: 'Hoover',
      acquired: '2007',
      based: 'U.S.',
      products: 'The iconic American vacuum cleaner brand, founded in 1908'
    },
    {
      company: 'Milwaukee',
      acquired: '2005',
      based: 'U.S.',
      products: 'Professional construction tools, founded in 1924'
    },
    {
      company: 'AEG',
      acquired: '2005',
      based: 'Germany',
      products: 'Professional construction tools; its roots go back to 1883'
    },
    {
      company: 'Dirt Devil',
      acquired: '2003',
      based: 'U.S.',
      products: 'Famous for its lightweight and handheld vacuum cleaners'
    },
    {
      company: 'Ryobi',
      acquired: '2000 to 2004',
      based: 'Japan',
      products: 'Makes and distributes the Japanese consumer brand in Australia, Europe, New Zealand and North America'
    },
    {
      company: 'Vax',
      acquired: '1999',
      based: 'U.K.',
      products: 'The U.K.s bestselling brand of floorcare products and sold in 50 markets worldwide'
    },
  ]

  useEffect(() => {
    setData(posts)
  })

  const style = { background: '#0092ff' }

  return (
    <div className="content-div">
      <p className="text-size-20 title bold">
        {data.length && data[97] && data[97].title ? _.capitalize(data[97].title) : ''}
      </p>
      <p className="text-size-15 italic">
        24 Feb 2021
      </p>
      <p>
        {
          data.map((post, id) => {
            // limit the blog content to 20 data
            if (id < 20) {
              if (id % 10 === 0) {
                return (
                  <>
                    {_.capitalize(post.body) + '. '}
                    <br /><br />
                  </>
                )
              } else {
                return (
                  _.capitalize(post.body) + '. '
                )
              }

            }
          })
        }
      </p>
      <div className="lower-div">
        <Row>
          <Col span={24}>
            {
              lowerData.map((item) => {
                return (
                  <>
                    <span className="text-size-15 bold">{item.company}</span><br />
                    <span className="text-size-12">Acquired: {item.acquired}</span><br />
                    <span className="text-size-12">Based: {item.based}</span><br />
                    <span className="text-size-12">Products: {item.products}</span>

                    <br />
                    <br />
                  </>
                )
              })
            }
          </Col>
        </Row>
      </div >
      <div className="share-div">
        <Row>
          <Col span={4}></Col>
          <Col span={4}>
            <span>
              <FacebookOutlined />
            </span>
          </Col>
          <Col span={4}>
            <span>
              <LinkedinOutlined />
            </span>
          </Col>
          <Col span={4}>
            <span>
              <TwitterOutlined />
            </span>
          </Col>
          <Col span={4}>
            <span>
              <MailOutlined />
            </span>
          </Col>
        </Row>
      </div >
    </div>
  )
}

export default Content
