import React from 'react'
import { Row, Col, Divider } from 'antd';
import '../styles/footer.css'

function FooterComp() {

  const data = [
    {
      id: 0,
      title: 'Our Company',
      description: [
        'About TTI',
        'Strategic Drivers',
        'Our Board',
        'TTI Worldwide',
        'History'
      ]
    },
    {
      id: 1,
      title: 'Our Business',
      description: [
        'Divisions',
        'Brands'
      ]
    },
    {
      id: 2,
      title: 'Investor Relations',
      description: [
        'Consolidated Financial',
        'Results',
        'Stock Quote & Share',
        'Information',
        'Announcements',
        'Press Releases',
        'Financial Reports',
        'Presentations',
        'Webcasts',
        'Circulars & Proxy',
        'Forms',
        'Shareholder Services',
        'Financial Calendar',
        'Corporate Governance'
      ]
    },
    {
      id: 3,
      title: 'Corporate Governance',
      description: [
        'Board of Directos',
        'Board Coommittees',
        'Codes & Policies',
        'Accountability & Audit'
      ]
    },
    {
      id: 4,
      title: 'Sustainability',
      description: [
        'CEO Message',
        'ESG Reports',
        'Environment',
        'Social',
        'Governance'
      ]
    },
    {
      id: 5,
      title: 'Careers',
      description: [
        'Working at TTI',
        'Leadership',
        'Development',
        'Global Career',
        'Opportunities'
      ]
    },
    {
      id: 6,
      title: 'Press Room',
      description: [
        'Press Releases',
        'Media Coverage',
        'Brand News',
        'Brand Media',
        'Brand Social',
        'Media Enquiry'
      ]
    },
    {
      id: 7,
      title: 'Contacts',
      description: [
        'TTI Offices',
        'Media Enquiry',
        'General Enquiry',
        'IR Contact',
        'Partner With Us'
      ]
    }
  ]

  return (
    <div className="footer-div">
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="footer-row">
        <Row>
          <h1 style={{ color: 'white' }}>Explore TTI</h1>
        </Row>
        <Divider
          id="footer-divider"
          style={
            {
              borderColor: 'white',
              minWidth: '100% !important',
              maxWidth: '100% !important',
              margin: 0
            }
          }
        />
        <br />
        <Row gutter={24} className="row-1 text-size-10">
          <Col className="gutter-row" span={8}>
            <Row>
              <span className="bolder">{data[0].title}</span>
            </Row>
            {
              data[0].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
          <Col className="gutter-row" span={6}>
            <Row>
              <span className="bolder">{data[1].title}</span>
            </Row>
            {
              data[1].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
          <Col className="gutter-row" span={9} offset={1}>
            <Row>
              <span className="bolder">{data[2].title}</span>
            </Row>
            {
              data[2].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
        </Row>
        <Row gutter={24} className="row-2 text-size-10">
          <Col className="gutter-row" span={8}>
            <Row>
              <span className="bolder">{data[3].title}</span>
            </Row>
            {
              data[3].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
          <Col className="gutter-row" span={4}>
            <Row>
              <span className="bolder">{data[4].title}</span>
            </Row>
            {
              data[4].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
          <Col className="gutter-row" span={10} offset={2}>
            <Row>
              <span className="bolder">{data[5].title}</span>
            </Row>
            {
              data[5].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
        </Row>
        <Row gutter={24} className="row-3 text-size-10">
          <Col className="gutter-row" span={12}>
            <Row>
              <span className="bolder">{data[6].title}</span>
            </Row>
            {
              data[6].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
          <Col className="gutter-row" span={10} offset={2}>
            <Row>
              <span className="bolder">{data[7].title}</span>
            </Row>
            {
              data[7].description.map((item) => {
                return (
                  <>
                    <span>{item}</span><br />
                  </>
                )
              })
            }
          </Col>
        </Row>
      </Row>
    </div>
  )
}

export default FooterComp
