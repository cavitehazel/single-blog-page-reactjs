import { MenuOutlined } from '@ant-design/icons'
import { Button } from 'antd'
import React, {
  useState,
  useEffect
} from 'react'
import '../styles/header.css'

function Header(props) {

  const { collapsedMenu, toggleCollapsed } = props

  useEffect(() => {
  }, [collapsedMenu])

  return (
    <div className="header-div">
      <Button
        className="toggle-btn"
        onClick={toggleCollapsed}
        style={{display: collapsedMenu === false ? 'none' : ''}}
      >
        <MenuOutlined />
      </Button>
    </div>
  )
}

export default Header
