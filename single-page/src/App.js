import './App.css'
import React, {
  useEffect,
  useState
} from 'react'
import 'antd/dist/antd.css'
import 'antd/dist/antd.less'
import { Layout } from 'antd'
import FetchContent from './services/fetchContent'
import FetchSider from './services/fetchSider'
import HeaderComp from './components/HeaderComp.js'
import FooterComp from './components/FooterComp'

const { Header, Footer, Sider, Content } = Layout;

function App() {
  const [collapsedMenu, setCollapsedMenu] = useState(true)
  const [menuStyle, setMenuStyle] = useState('none')

  useEffect(() => {
  }, [collapsedMenu, menuStyle])

  const toggleCollapsed = () => {
    setCollapsedMenu(!collapsedMenu)
    updateStyle()
  }

  const updateStyle = () => {
    return collapsedMenu === false ? setMenuStyle('none') : setMenuStyle('')
  }

  return (
    <div className="App">
      <>
        <Layout>
          <Sider className="side-bar" style={{ display: menuStyle }} >
            <FetchSider
              collapsedMenu={collapsedMenu}
              toggleCollapsed={toggleCollapsed}
            />
          </Sider>
          <Layout>
            <Header>
              <HeaderComp
                collapsedMenu={collapsedMenu}
                toggleCollapsed={toggleCollapsed}
              />
            </Header>
            <Content>
              <FetchContent />
            </Content>
          </Layout>
        </Layout>
        <Footer>
          <FooterComp />
        </Footer>
      </>
    </div>
  );
}

export default App;